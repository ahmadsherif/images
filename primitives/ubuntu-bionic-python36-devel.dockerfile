FROM registry.gitlab.com/datadrivendiscovery/images/core:ubuntu-bionic-python36-devel

# This assumes "build.sh" cloned the repository.
COPY ./primitives /primitives
COPY ./install-primitives.py /install-primitives.py

# In devel image we install primitives which were made for a stable version.
# They can install a stable version of the core package, so after installing
# all primitives we reinstall the devel version of the core package.
# Furthermore, we do not check for consistent dependencies because they
# probably are not because of us forcing the devel version of the core package.
RUN cd / && \
 python3 /install-primitives.py && \
 rm -rf /primitives /install-primitives.py && \
 pip3 install git+https://gitlab.com/datadrivendiscovery/d3m.git@devel#egg=d3m && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm
