FROM registry.gitlab.com/datadrivendiscovery/images/core:ubuntu-bionic-python36-v2019.4.4

# This assumes "build.sh" cloned the repository.
COPY ./primitives /primitives
COPY ./install-primitives.py /install-primitives.py

# After installing all primitives, we check that the image has consistent dependencies.
# The goal here is that image building fails if this is not true.
RUN cd / && \
 python3 /install-primitives.py && \
 rm -rf /primitives /install-primitives.py && \
 pip3 check && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm
