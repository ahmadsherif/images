FROM ubuntu:bionic

ENV DEBIAN_FRONTEND=noninteractive

# apt-utils seems missing and warnings are shown, so we install it.
RUN apt-get update -q -q && \
 apt-get install --yes --force-yes apt-utils tzdata locales file sudo && \
 echo 'UTC' > /etc/timezone && \
 rm /etc/localtime && \
 dpkg-reconfigure tzdata && \
 apt-get upgrade --yes --force-yes && \
 rm -f /etc/cron.weekly/fstrim && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Prevent Python packages to be installed through APT.
COPY ./etc/apt/preferences.d/no-python2 /etc/apt/preferences.d/no-python2

# General dependencies for building dependencies, locales, and some utilities.
RUN sed -i 's/^# deb-src/deb-src/' /etc/apt/sources.list && \
 apt-get update -q -q && \
 apt-get install --yes --force-yes --no-install-recommends swig git build-essential cmake wget ssh tar gzip ca-certificates unzip curl libcurl4-openssl-dev libssl-dev equivs vim-tiny && \
 locale-gen --no-purge en_US.UTF-8 && \
 update-locale LANG=en_US.UTF-8 && \
 echo locales locales/locales_to_be_generated multiselect en_US.UTF-8 UTF-8 | debconf-set-selections && \
 echo locales locales/default_environment_locale select en_US.UTF-8 | debconf-set-selections && \
 dpkg-reconfigure locales && \
 update-alternatives --install /usr/bin/vim vim /usr/bin/vim.tiny 10 && \
 curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash && \
 apt-get install --yes --force-yes git-lfs && \
 git lfs install && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# Docker in Docker.
RUN apt-get update -q -q && \
 apt-get install --yes --force-yes apt-transport-https ca-certificates && \
 curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add - && \
 echo "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable" > /etc/apt/sources.list.d/docker.list && \
 apt-get update -q -q && \
 apt-get install --yes --force-yes docker-ce && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# Set locale to UTF-8 which makes Python read text in UTF-8 and not ASCII, by default.
ENV LC_ALL=en_US.UTF-8

# Installing Python 3.6 and pip 19.0.3.
RUN apt-get update -q -q && \
 apt-get install --yes --force-yes python3.6 python3.6-dev python3-pip python3-openssl && \
 pip3 install pip==19.0.3 && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# Base Python packages (those dependencies from d3m core package which are needed also in testing image).
RUN pip3 install docker[tls]==2.7 && \
 pip3 install --upgrade setuptools && \
 pip3 install frozendict==1.2 && \
 pip3 install PyYAML==3.12 && \
 pip3 install pycurl==7.43.0.1 && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# Dependencies needed to build Python itself.
RUN apt-get update -q -q && \
 apt-get build-dep --yes --force-yes python3.6 && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# Extra Python dependencies.
RUN apt-get update -q -q && \
 apt-get install --yes --force-yes --no-install-recommends zlib1g-dev libexpat1-dev tk8.6-dev libffi-dev libssl-dev \
 libbz2-dev liblzma-dev libncurses5-dev libreadline6-dev libsqlite3-dev libgdbm-dev liblzma-dev lzma lzma-dev && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# Dummy package to satisfy unnecessary dependency on Python 2.
# See: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=891712
COPY ./equivs/python-dev /tmp/python-dev
RUN cd /tmp && \
 equivs-build python-dev && \
 dpkg -i python-dev-dummy_1.0_all.deb && \
 cd / && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# Install CUDA 9.0.
# Based on:
# https://gitlab.com/nvidia/cuda/blob/ubuntu16.04/9.0/base/Dockerfile
# https://github.com/tensorflow/tensorflow/blob/master/tensorflow/tools/dockerfiles/dockerfiles/gpu.Dockerfile
ENV CUDA_VERSION=9.0.176
ENV CUDNN_VERSION=7.4.1.5-1
ENV NCCL_VERSION=2.4.2
ENV CUDA_PKG_VERSION=9-0=$CUDA_VERSION-1
RUN \
 curl -fsSL https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/7fa2af80.pub | apt-key add - && \
 echo "deb https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64 /" > /etc/apt/sources.list.d/cuda.list && \
 echo "deb https://developer.download.nvidia.com/compute/machine-learning/repos/ubuntu1604/x86_64 /" > /etc/apt/sources.list.d/nvidia-ml.list && \
 apt-get update -q -q && \
 apt-get install --yes --force-yes --no-install-recommends \
  cuda-cudart-$CUDA_PKG_VERSION \
  cuda-command-line-tools-$CUDA_PKG_VERSION \
  cuda-cublas-$CUDA_PKG_VERSION \
  cuda-cufft-$CUDA_PKG_VERSION \
  cuda-curand-$CUDA_PKG_VERSION \
  cuda-cusolver-$CUDA_PKG_VERSION \
  cuda-cusparse-$CUDA_PKG_VERSION \
  libcudnn7=$CUDNN_VERSION+cuda9.0 \
  libfreetype6-dev \
  libhdf5-serial-dev \
  libzmq3-dev \
  pkg-config \
  software-properties-common \
  nvinfer-runtime-trt-repo-ubuntu1604-5.0.2-ga-cuda9.0 \
  cuda-libraries-dev-$CUDA_PKG_VERSION \
  cuda-nvml-dev-$CUDA_PKG_VERSION \
  cuda-minimal-build-$CUDA_PKG_VERSION \
  cuda-core-$CUDA_PKG_VERSION \
  cuda-cublas-dev-$CUDA_PKG_VERSION \
  libnccl2=$NCCL_VERSION-1+cuda9.0 \
  libnccl-dev=$NCCL_VERSION-1+cuda9.0 && \
 apt-get update -q -q && \
 apt-get install --yes --force-yes --no-install-recommends libnvinfer5=5.0.2-1+cuda9.0 && \
 ln -s cuda-9.0 /usr/local/cuda && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# Install TensorFlow GPU version, and appropriate versions of Keras, Theano, Pytorch.
RUN pip3 install \
 https://storage.googleapis.com/tensorflow/linux/gpu/tensorflow_gpu-1.12.0-cp36-cp36m-linux_x86_64.whl \
 keras==2.2.4 \
 Theano==1.0.4 \
 torch==1.0.0 && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

LABEL com.nvidia.volumes.needed="nvidia_driver"
LABEL com.nvidia.cuda.version="${CUDA_VERSION}"

RUN \
 echo "/usr/local/nvidia/lib" >> /etc/ld.so.conf.d/nvidia.conf && \
 echo "/usr/local/nvidia/lib64" >> /etc/ld.so.conf.d/nvidia.conf && \
 echo "/usr/local/cuda/lib64" >> /etc/ld.so.conf.d/cuda.conf && \
 echo "/usr/local/cuda-9.0/lib64" >> /etc/ld.so.conf.d/cuda.conf && \
 echo "/usr/local/cuda/extras/CUPTI/lib64" >> /etc/ld.so.conf.d/cuda.conf && \
 ldconfig

# Make sure that we can run also without GPU.
RUN \
 ln -s /usr/local/cuda/lib64/stubs/libcuda.so /usr/local/cuda/lib64/stubs/libcuda.so.1 && \
 echo "/usr/local/cuda/lib64/stubs" >> /etc/ld.so.conf.d/zzz-cuda-stubs.conf && \
 ldconfig

ENV PATH=/usr/local/nvidia/bin:/usr/local/cuda/bin:${PATH}

ENV NVIDIA_VISIBLE_DEVICES=all
ENV NVIDIA_DRIVER_CAPABILITIES=compute,utility
ENV NVIDIA_REQUIRE_CUDA="cuda>=9.0"
